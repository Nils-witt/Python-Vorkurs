import operator
import sys

ops = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.truediv,
}


def calc():
    x = int(input("1. Zahl: "))
    y = int(input("2. Zahl: "))
    operator = input("Rechenart: ")

    if ops.get(operator) is None:
        print("Ungültiger Operator")
    else:
        print("Ergebnis: " + str(ops[operator](x, y)))


if __name__ == "__main__":
    try:
        while True:
            try:
                calc()
                print("------------------------------")
            except KeyboardInterrupt:
                pass
            except ValueError:
                print("Eingabefehler")
    except ZeroDivisionError:
        print("Err: Durch 0 geteilt")
        sys.exit(0)
