from pprint import pprint


def main():
    text = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam' \
           'nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo' \
           'duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'

    char_dict = {i.lower(): text.count(i.lower()) + text.count(i.upper()) for i in text if
                 (i != ' ' and i != ',' and i != '.')}

    pprint(char_dict)


if __name__ == '__main__':
    main()
