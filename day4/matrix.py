from pprint import pprint


def main():
    matrix = [[1 if x == 2 else 0 for x in range(0, 5)] if k != 2 else [1 for x in range(0, 5)] for k in range(0, 5)]

    pprint(matrix)


if __name__ == '__main__':
    main()
