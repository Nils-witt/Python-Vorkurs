import csv
from pprint import pprint

import requests


def more_then(min):
    with open('covid19-data.csv', newline='') as csvdatei:
        csvinhalt = csv.DictReader(csvdatei, delimiter=';')

        for line in csvinhalt:
            if line['akut_erkrankt'] != "":
                if int(line['akut_erkrankt']) > min:
                    print(line)


def main():
    more_then(1000)


if __name__ == "__main__":
    main()
