import csv
from pprint import pprint


def sort():
    with open('covid19-data.csv', newline='') as csvdatei:
        csvinhalt = csv.DictReader(csvdatei, delimiter=';')

        sorted_list = sorted(csvinhalt, key=lambda k: k['datum'])

        return sorted_list


def main():
    print('Teil c)')
    pprint(sort())


if __name__ == "__main__":
    main()
