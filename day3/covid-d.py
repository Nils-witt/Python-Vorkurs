import covid


def max_increase():
    data = covid.sort()
    increase_day_max = ''
    increase_num_max = 0

    for i in range(1, len(data)):
        try:
            a = int(data[i - 1]["positiv_getest"])
            b = int(data[i]['positiv_getest'])

            if (b - a) > increase_num_max:
                increase_day_max = data[i]["datum"]
                increase_num_max = b - a
        except:
            # print("Unexpected error:", sys.exc_info()[0])
            pass

    print(increase_day_max + " => " + str(increase_num_max))


def main():
    max_increase()


if __name__ == "__main__":
    main()
