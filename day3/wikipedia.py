import json

import requests


def getLinks(begriff):
    res = requests.get(
        "https://de.wikipedia.org/w/api.php?action=query&prop=links&pllimit=max&format=json&titles=" + begriff)
    data = json.loads(res.text)
    pages = data['query']['pages']

    page_ids = data['query']['pages'].keys()

    links = []

    for id in page_ids:
        page_links = pages[id]['links']
        for link in page_links:
            links.append(link['title'])

    return links


def main():
    start = input("Start: ")
    ziel = input("Ziel: ")

    current_page = start

    while current_page != ziel:

        current_dict = {}
        for idx, title in enumerate(getLinks(current_page)):
            print(idx, title)
            current_dict[idx] = title

        auswahl_id = int(input("Auswahl: "))
        current_page = current_dict[auswahl_id]

    print("Fertig")

if __name__ == "__main__":
    main()
