import requests


def download():
    data = requests.get('http://luksab.de/covid19-data.csv')
    if data.status_code == 200:
        file = open('covid19-data.csv', 'w')
        file.write(data.text)
        file.close()
        print("Done")

    else:
        print(data.status_code)


def main():
    download()


if __name__ == "__main__":
    main()
