import csv
from pprint import pprint

import requests


def more_then(min):
    with open('covid19-data.csv', newline='') as csvdatei:
        csvinhalt = csv.DictReader(csvdatei, delimiter=';')

        for line in csvinhalt:
            try:
                if int(line['akut_erkrankt']) > min:
                    print(line)
            except:
                pass


def sort():
    with open('covid19-data.csv', newline='') as csvdatei:
        csvinhalt = csv.DictReader(csvdatei, delimiter=';')

        sorted_list = sorted(csvinhalt, key=lambda k: k['datum'])

        return sorted_list


def download():
    data = requests.get('http://luksab.de/covid19-data.csv')
    if data.status_code == 200:
        file = open('covid19-data.csv', 'w')
        file.write(data.text)
        file.close()
        print("Done")

    else:
        print(data.status_code)


def max_increase():
    data = sort()
    increase_day_max = ''
    increase_num_max = 0

    for i in range(1, len(data)):
        try:
            a = int(data[i - 1]["positiv_getest"])
            b = int(data[i]['positiv_getest'])

            if (b - a) > increase_num_max:
                increase_day_max = data[i]["datum"]
                increase_num_max = b - a
        except:
            # print("Unexpected error:", sys.exc_info()[0])
            pass

    print(increase_day_max + " => " + str(increase_num_max))


def main():
    print("Teil a)")
    download()
    print('Teil b)')
    more_then(1000)
    print('Teil c)')
    pprint(sort())
    print('Teil d)')
    max_increase()


if __name__ == "__main__":
    main()
