import numpy


def main():
    laengearray = 0
    while laengearray != 5:
        ints1 = numpy.random.randint(low=0, high=10, size=10)
        ints2 = numpy.random.randint(low=0, high=10, size=10)

        laengearray = numpy.sum(ints1 == ints2)
        # laengearray = numpy.sum(numpy.where(ints1 == ints2, True, False))

    print(ints1)
    print(ints2)


if __name__ == '__main__':
    main()
