import mypy


def flaecheninhalt(breite: float, laenge: float) -> float:
    return breite * laenge


def zeichenzaehlen(text: str):
    return {character: text.count(character) for character in text}


def fancy_function(namen_und_matrikelnummern: list[tuple[str, int]], semester: int, bestanden: bool = False):
    for eintrag in namen_und_matrikelnummern:
        print(f'Student {eintrag[0]} ({eintrag[1]}) ')
        print(f'Im {semester}. Semester hat ')
        print(f'{" nicht " if not bestanden else ""} bestanden .')


def main():
    mypy
    pass


if __name__ == '__main__':
    main()
