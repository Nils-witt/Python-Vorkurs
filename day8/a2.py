def ngrams(text: str, n: int):
    text = text.replace(".", "").replace(",", "").replace("!", "")

    liste = text.split(" ")

    return [tuple([liste[i] for i in range(i, i + n)]) for i in range(0, len(liste) - n)]


def main():
    print(ngrams('I read a book .', 2))
    yield


if __name__ == '__main__':
    main()
