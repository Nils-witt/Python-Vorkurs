def main():
    sortedlist = sorted(['az', 'Az', 'a', 'A', 'g', 's', 'z'], key=lambda k: k.lower(), reverse=True)
    print(sortedlist)


if __name__ == '__main__':
    main()
