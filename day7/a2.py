import pickle
import random


class Geldboerse:

    def __init__(self):
        self.einer = random.randint(0, 5)
        self.zweier = random.randint(0, 5)

        self.fuenfer = random.randint(0, 5)
        self.zehner = random.randint(0, 5)
        self.zwanziger = random.randint(0, 5)
        self.fuenfziger = random.randint(0, 5)

    def value(self):
        return self.einer + self.zweier * 2 + self.fuenfer * 5 + self.zehner * 10 + self.zwanziger * 20 + self.fuenfziger * 50

    def __str__(self):
        return "Es sind " + str(self.value()) + " in der Geldboerse"

    def __eq__(self, other):
        return self.value() == other.value()


def main():
    gb = Geldboerse()
    gb2 = Geldboerse()
    print(gb)
    print(gb.__eq__(gb2))

    outfile = open('boerse', 'wb')

    pickle.dump(gb, outfile)
    outfile.close()

    infile = open('boerse', 'rb')
    gb_l = pickle.load(infile)
    infile.close()
    print(gb_l)


if __name__ == '__main__':
    main()
