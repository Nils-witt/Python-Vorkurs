import datetime


def main():
    datum = datetime.datetime.fromisoformat(input("Datum (YYYY-mm-dd): "))

    datum = datum.replace(day=1)
    datum = (datum + datetime.timedelta(days=35)).replace(day=1)
    datum = (datum - datetime.timedelta(days=1))

    print("Letzter Tag des Monats: ", datum.day, ".")


if __name__ == "__main__":
    main()
