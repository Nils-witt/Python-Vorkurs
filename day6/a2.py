import argparse
import datetime


def main():
    parser = argparse.ArgumentParser(description='Input Birthday')
    parser.add_argument('date', metavar='N', type=str)

    args = parser.parse_args()

    datum1 = datetime.date.fromisoformat(args.date)
    datum2 = datetime.date.today()
    diffObj = datum2 - datum1
    # Wochen
    print("Wochen: " + str((diffObj.days) / 7))
    # Tage
    print("Tage: " + str(diffObj.days))
    # Stunden
    print("Stunden: " + str(diffObj.days * 24))
    # Sekunden
    print("Sekunden: " + str(diffObj.days * 24 * 3600))


if __name__ == '__main__':
    main()
