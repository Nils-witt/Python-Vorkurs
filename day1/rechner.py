import operator

ops = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.truediv,
}

if __name__ == "__main__":
    x = int(input("1. Zahl: "))
    y = int(input("2. Zahl: "))
    operator = input("Rechenart: ")

    if ops.get(operator) is None:
        print("Ungültiger Operator")
    else:
        print(ops[operator](x, y))
