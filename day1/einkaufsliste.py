if __name__ == "__main__":
    alice = ['Bananen', 'Brot', 'Schokolade', 'Rotwein']
    bob = ['Bier', 'Bier', 'Chips']
    eve = ['Brot', 'Schokolade', 'Chips', 'Bier', 'Wasser']

    geslist = alice + bob + eve

    gesdict = dict()

    for item in geslist:
        gesdict[item] = gesdict.get(item, 0) + 1

    print(gesdict)

    for key in gesdict:
        print(key + ": " + str(gesdict[key]))
