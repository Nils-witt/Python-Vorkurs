from random import randint


def main():
    numbers = []
    for i in range(0, 20):
        numbers.append(randint(0, 300))

    print(numbers)

    pos_change = True

    while pos_change:
        pos_change = False
        for idx in range(0, len(numbers) - 1):
            a = numbers[idx]
            b = numbers[idx + 1]
            if a > b:
                numbers[idx] = b
                numbers[idx + 1] = a
                pos_change = True

    print(numbers)


if __name__ == "__main__":
    main()
