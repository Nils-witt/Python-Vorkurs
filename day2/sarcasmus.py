from random import randint as zufallszahl


def sarcasmus(text, factor):
    out = ''

    for idx, item in enumerate(text):
        if (zufallszahl(0, 101) < factor):
            out += text[idx].upper()
        else:
            out += text[idx]
    return out


def main():
    text = input("String: ")
    factor = int(input("Faktor: (0-100)"))
    print(sarcasmus(text, factor))


if __name__ == "__main__":
    main()
