class Geburtstag:

    def __init__(self, day, month, year):
        self.day = str(day)
        self.month = str(month)
        self.year = str(year)

    def __str__(self):
        return self.day.zfill(2) + ". " + self.month + " " + self.year


def main():
    gb = Geburtstag(1, 'Januar', 1900)

    print(gb)


if __name__ == "__main__":
    main()
