import random

# Wenn man iwas nachgucken möchte : https://www.w3schools.com/python/

cells = {}


def print_cells(rows, cols):
    ausgabe = ""
    for i in range(0, rows + 1):
        for j in range(0, cols + 1):
            ausgabe += cells.get((i, j), None).__str__()
        ausgabe += "\n"
    ausgabe += " - " * cols + "\n"
    print(ausgabe)


class Cell:
    def __init__(self, rows=None, cols=None):
        self.rows = rows
        self.cols = cols
        self.cell_id = (rows, cols)
        self.value = 0
        self.next_gen_value = 0
        pass

    # set cell.status: dead or alive
    def set_value(self):
        self.value = random.randint(0, 1)

    def check_neighbor(self) -> int:
        sum = 0
        for row in range(self.rows - 1, self.rows + 2):
            for column in range(self.cols - 1, self.cols + 2):
                sum += cells.get((row, column), Cell()).value

        sum -= self.value  # very very smart
        return sum

    def update_worklifebalance(self):
        neighbor_value = self.check_neighbor()
        if (self.value == 1) and (neighbor_value == 3 or neighbor_value == 2):
            self.next_gen_value = 1
        elif self.value == 0 and neighbor_value == 3:
            self.next_gen_value = 1
        else:
            self.next_gen_value = 0

    def update_new_gen_value(self):
        self.value = self.next_gen_value

    def __str__(self):
        if self.value == 1:
            s = "⬛ "
        else:
            s = "⬜ "
        return s


def main():
    testing = True
    if testing:
        rows = 10
        cols = 10
        gens = 10
    else:
        rows = int(input("Anzahl Rows: "))
        cols = int(input("Anzahl Cols: "))
        gens = int(input("Anzahl Generationen: "))

    # Spielfeld initialisieren
    for i in range(0, rows + 1):
        for j in range(0, cols + 1):
            key = (i, j)
            cells[key] = Cell(i, j)

    for x in range(0, gens):
        for cell in cells:
            cells[cell].set_value()
        for cell in cells:
            cells[cell].update_worklifebalance()
        for cell in cells:
            cells[cell].update_new_gen_value()

        print_cells(rows, cols)


if __name__ == '__main__':
    main()
